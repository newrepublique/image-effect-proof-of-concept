
Caman.Event.listen("renderFinished", function (job) {
  $('.progress .current').empty()
});


function sepia(){
  Caman.Event.listen("processComplete", function (job) {

    $('.progress .current').empty()
    $('.progress .current').append("Applying: " + job.name)

  });
  $('.sepia').css({
    'background': '#eb7a00'
  })
  Caman(".sepia", function () {
      this.vibrance(5)
      this.exposure(10)
      this.sepia(100)
      this.saturation(-40)
      this.colorize("#ee7a21", 15)
      this.curves('rgb', [110, 120], [255, 255], [255, 255], [255, 255]);
      this.newLayer(function () {
          
          this.setBlendingMode("multiply");
          this.opacity(75)
          this.copyParent();

          this.filter.greyscale()
          this.filter.contrast(100);
        });
      this.noise(4)
      
      this.render()
    });
}

function duotune(){
  Caman.Event.listen("processComplete", function (job) {
    $('.progress .current').empty()
    $('.progress .current').append("Applying:" + job.name)
  });
  $('.duotune').css({
    'background': '#687cb6'
  })
  Caman(".duotune", function () {
      this.greyscale()
      this.curves('rgb', [110, 40], [255, 255], [255, 255], [255, 255]);
      this.brightness(50)
      this.curves('rgb', [180, 0], [255, 255], [255, 255], [255, 255]);
      
      this.curves('rgb', [0, 10], [255, 255], [255, 255], [255, 255]);
      this.curves('rgb', [90, 0], [255, 255], [255, 255], [255, 255]);
      this.colorize("#677ab7", 100)
      this.newLayer(function () {

          this.setBlendingMode("multiply");

          this.copyParent();

          this.filter.greyscale()
          this.filter.contrast(100);
        });
      this.render()
});
}



function highContrast(){
  Caman.Event.listen("processComplete", function (job) {
    $('.progress .current').empty()
    $('.progress .current').append("Applying:" + job.name)
  });

  $('.highContrast').css({
    'background': '#000'
  })

  Caman(".highContrast", function () {
      this.greyscale()
      this.curves('rgb', [110, 40], [255, 255], [255, 255], [255, 255]);
      this.brightness(50)
      this.curves('rgb', [180, 0], [255, 255], [255, 255], [255, 255]);
      this.render()
    });
}

function fifties2(){
  Caman.Event.listen("processComplete", function (job) {
    $('.progress .current').empty()
    $('.progress .current').append("Applying:" + job.name)
  });
  Caman(".fifties", function () {
      this.saturation(-40)
      this.contrast(-5)
      this.newLayer(function () {

          this.setBlendingMode("overlay");

          this.fillColor('#5d3717');
          this.opacity(50)

        });
      this.exposure(20)
      this.curves('rgb', [20, 0], [255, 255], [255, 255], [255, 255]);
      
      this.newLayer(function () {
          
          this.setBlendingMode("lighten");
          this.fillColor('#5d3717');
          this.opacity(20)
        });
      this.newLayer(function () {
          
          this.setBlendingMode("overlay");
          this.fillColor('#fb8c5b');
          this.opacity(5)
        });
      this.newLayer(function () {
          
          this.setBlendingMode("overlay");
          this.fillColor('#fb8c5b');
          this.opacity(5)
        });
      

      this.sharpen(10)
      this.curves('rgb', [20, 20], [255, 255], [255, 255], [255, 255]);
      this.curves('rgb', [0, 30], [255, 255], [255, 255], [255, 255]);
      this.contrast(5)
      this.render()
    });
}

function fifties(){
  Caman.Event.listen("processComplete", function (job) {
    $('.progress .current').empty()
    $('.progress .current').append("Applying:" + job.name)
  });
  $('.fifties').css({
    'background': '#d5a273'
  })
  Caman(".fifties", function () {
      this.saturation(-40)
      this.newLayer(function(){
        this.setBlendingMode("multiply");
        this.copyParent()
        this.filter.saturation(-100)
        this.filter.stackBlur(30)
        this.opacity(40)
        this.filter.contrast(100)
        this.filter.greyscale
      })
      this.newLayer(function(){
        this.setBlendingMode("overlay");
        this.copyParent()
        this.filter.saturation(-100)
        this.filter.stackBlur(30)
        this.opacity(40)
        this.filter.contrast(100)
        this.filter.greyscale
      })
      this.newLayer(function () {
          
          this.setBlendingMode("overlay");
          this.fillColor('#fb8c5b');
          this.opacity(30)
        });

      this.vibrance(80)

      // shadows  
      this.newLayer(function () {
          this.setBlendingMode("multiply");
          this.copyParent()
          this.opacity(20)
          this.filter.saturation(-100)
          this.filter.contrast(350)
          this.filter.stackBlur(0.5)
        });

      // highlights
     this.newLayer(function () {
         this.setBlendingMode("overlay");
         this.copyParent()
         this.opacity(20)
         this.filter.saturation(-100)
         this.filter.contrast(300)
         this.filter.stackBlur(0.5)
       });
  

      this.curves('rgb', [140, 50], [255, 255], [255, 255], [255, 255]);

      this.sharpen(30)
      this.noise(3)
      this.render()


    });
}


function posterise(){
  $('.posterise').attr('data-pb-posterize-amount', '4')
  $('.posterise').attr('data-pb-posterize-opacity', '0.5')
  processFilters();
}


